# Description:
#   coala is always written with a lower case c.
#
# Author:
#   ChauffeR

module.exports = (robot) ->

  robot.hear /.*C+[oO]+[aA]+[lL]+[aA]+.*/, (msg) ->

    emoji = ":expressionless: :disappointed_relieved: :open_mouth: :confounded: :anguished: :frowning: :cold_sweat: :no_mouth: :angry: :triumph:".split(" ")
    rand_emoji = emoji[(Math.random() * emoji.length) | 0]

    msg.send "coala is always written with a lower case c. #{rand_emoji}"
